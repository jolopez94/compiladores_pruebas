#include "parser.h"

using token_list = std::list<Token>;
static token_list EPSILON;

token_list Parser::getProduction(Token topStack) {
    switch (topStack) {
        case Token::Stmt_list:
            switch (token) {
                case Token::Ident: return {Token::Stmt, Token::Semicolon, Token::Stmt_list};
                case Token::KwPrint: return {Token::Stmt, Token::Semicolon, Token::Stmt_list};
                case Token::Eof: return EPSILON;
            }
        break;
        case Token::Stmt:
            switch (token) {
                case Token::Ident: return {Token::Ident, Token::OpAssign, Token::Expr};
                case Token::KwPrint: return {Token::KwPrint, Token::Expr};
            }
        break;
        case Token::Expr:
            switch (token) {
                case Token::OpenPar: return {Token::Term, Token::ExprP};
                case Token::Number: return {Token::Term, Token::ExprP};
                case Token::Ident: return {Token::Term, Token::ExprP};
            }
        break;
        case Token::ExprP:
            switch (token) {
                case Token::Semicolon: return EPSILON;
                case Token::OpAdd: return {Token::OpAdd, Token::Term, Token::ExprP};
                case Token::ClosePar: return EPSILON;
            }
        break;
        case Token::Term:
            switch (token) {
                case Token::OpenPar: return {Token::Factor, Token::TermP};
                case Token::Number: return {Token::Factor, Token::TermP};
                case Token::Ident: return {Token::Factor, Token::TermP};
            }
        break;
        case Token::TermP:
            switch (token) {
                case Token::Semicolon: return EPSILON;
                case Token::OpAdd: return EPSILON;
                case Token::OpMult: return {Token::OpMult, Token::Factor, Token::TermP};
                case Token::ClosePar: return EPSILON;
            }
        break;
        case Token::Factor:
            switch (token) {
                case Token::OpenPar: return {Token::OpenPar, Token::Expr, Token::ClosePar};
                case Token::Number: return {Token::Number};
                case Token::Ident: return {Token::Ident};
            }
        break;
    }
    throw "Error";
}

void Parser::parse() {
    stack.push_front(Token::Eof);
    stack.push_front(Token::Stmt_list);
    token = lexer.getNextToken();

    while (!stack.empty()) {
        if (lexer.isTerminal(stack.front())) {
            if (token == stack.front()) {
                std::cout << lexer.getText() << std::endl;
                token = lexer.getNextToken();
                stack.pop_front();
            } else {
                throw "Error";
            }
        } else {
            token_list productions = getProduction(stack.front());
            productions.reverse();
            stack.pop_front();
            
            for (auto const& i: productions) {
                stack.push_front(i);
            }
        }
    }
    std::cout << "File parsed succesfully" << std::endl;
}
