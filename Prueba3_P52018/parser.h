#ifndef _PARSER_H
#define _PARSER_H

#include <list>

#include "lexer.h"

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    void parse();

private:
    Lexer& lexer;
    Token token;
    std::list<Token> stack;

    void printStack();
    std::list<Token> getProduction(Token topStack);
};

#endif
