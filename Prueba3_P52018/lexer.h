#ifndef _LEXER_H
#define _LEXER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

enum class Token: unsigned int { 
    KwPrint = 1,
    Ident = 2,
    Semicolon = 3,
    OpenPar = 4, 
    ClosePar = 5, 
    OpAssign = 6,
    OpAdd = 7, 
    OpMult = 8, 
    Number = 9, 
    Unknown = 10, 
    Eof = 11,
    Stmt_list = 256,
    Stmt = 257,
    Expr = 258,
    ExprP = 259,
    Term = 260,
    TermP = 261,
    Factor = 262
};

using TokenInfo = std::pair<Token, std::string>;

class Lexer {
public:
	Lexer(const std::vector<TokenInfo>& tklst): tklist(tklst) {
        it = tklist.begin();
    }

    Token getNextToken() {
        Token tk = it->first;
        text = it->second;
        
        if (it != tklist.end()) {
            it++;
        }
        
        return tk;
    }
    
    bool hasTokens() { return it != tklist.end(); }
    std::string getText() { return text; }
    bool isTerminal (Token tk) {
        return static_cast<unsigned int>(tk) < 255;
    }
private:
    std::string text;
    std::vector<TokenInfo> tklist;
    std::vector<TokenInfo>::iterator it;
};
#endif
