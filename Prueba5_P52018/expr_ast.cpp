#include "expr_ast.h"

std::string BinaryExpr::toString() {
    std::string s1 = expr1->toString();
    std::string s2 = expr2->toString();

    if (getPrec() > expr1->getPrec()) {
        s1 = "(" + s1 + ")";
    }
    if (getPrec() > expr2->getPrec()) {
        s2 = "(" + s2 + ")";
    }

    return  s1 + getOper() + s2;
}