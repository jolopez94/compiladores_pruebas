#ifndef _AST_H
#define _AST_H

#include <string>
#include <memory>


class ASTNode {
public:
    virtual ~ASTNode() {}

    virtual std::string toString() = 0;
};

class Expr: public ASTNode {
public:
    virtual int getPrec() = 0;
};

using UPExpr = std::unique_ptr<Expr>;

class BinaryExpr: public Expr {
public:
    BinaryExpr(Expr *expr1, Expr *expr2):
        expr1(std::move(expr1)), expr2(std::move(expr2)) {}

    std::string toString() override;
    virtual std::string getOper() = 0;

    Expr *expr1;
    Expr *expr2;
};

class AddExpr: public BinaryExpr {
public:
    AddExpr(Expr *expr1, Expr *expr2):
    BinaryExpr(std::move(expr1), std::move(expr2)) {}

    std::string getOper() override { return "+"; }
    int getPrec() override { return 1; }
};

class SubExpr: public BinaryExpr {
public:
    SubExpr(Expr *expr1, Expr *expr2):
    BinaryExpr(std::move(expr1), std::move(expr2)) {}

    std::string getOper() override { return "-"; }
    int getPrec() override { return 1; }
};

class MulExpr: public BinaryExpr {
public:
    MulExpr(Expr *expr1, Expr *expr2):
    BinaryExpr(std::move(expr1), std::move(expr2)) {}

    std::string getOper() override { return "*"; }
    int getPrec() override { return 2; }
};

class DivExpr: public BinaryExpr {
public:
    DivExpr(Expr *expr1, Expr *expr2):
    BinaryExpr(std::move(expr1), std::move(expr2)) {}

    std::string getOper() override { return "/"; }
    int getPrec() override { return 2; }
};

class ModExpr: public BinaryExpr {
public:
    ModExpr(Expr *expr1, Expr *expr2):
    BinaryExpr(std::move(expr1), std::move(expr2)) {}

    std::string getOper() override { return "%"; }
    int getPrec() override { return 2; }
};

class NumExpr: public Expr {
public:
    NumExpr(int value) { this->value = value; }
    
    std::string toString() override { return "(" + std::to_string(value) + ")"; }
    int getPrec() override { return 3; }

    int value;
};

class IdExpr: public Expr {
public:
    IdExpr(std::string name) { this->name = name; }

    std::string toString() override { return "(" + name + ")"; }
    int getPrec() override { return 3; }

    std::string name;
};

#endif
