#include "parser.h"

void Parser::parse() {
    token = lexer.getNextToken();
    S();
    std::cout << lexer.getText() << std::endl;
    if (token == Token::Semicolon) {
        token = lexer.getNextToken();

        while (token == Token::Ident || token == Token::KwPrint) {
            S();
            if (token == Token::Semicolon) {
                token = lexer.getNextToken();
            } else {
                throw "Expected Semicolon";
            }
        }

        if (token == Token::Eof) {
            std::cout << "Filed parsed succesfully" << std::endl;
        } else {
            std::cout << "Failed" << std::endl;
        }
    } else {
        throw "Expected Semicolon";
    }
}

void Parser::S() {
    if (token == Token::Ident) {
        token = lexer.getNextToken();
        if (token == Token::OpAssign) {
            token = lexer.getNextToken();
            E();
        } else {
            std::cerr << "Expected assign" << std::endl;
        }
    } else if (token == Token::KwPrint) {
        token = lexer.getNextToken();
        E();
        if (token == Token::OpMult) {
            TP();
        }
    } else {
        std::cerr << "Expected ident or print" << std::endl;
    }
}

void Parser::E() {
    T();
    EP();
}

void Parser::EP() {
    if (token == Token::OpAdd) {
        token = lexer.getNextToken();
        T();
        EP();
    }
}

void Parser::T() {
    F();
    TP();
}

void Parser::TP() {
    if (token == Token::OpMult) {
        token = lexer.getNextToken();
        F();
        TP();
    }
}

void Parser::F() {
    if (token == Token::Ident || token == Token::Number) {
        token = lexer.getNextToken();
    } else if (token == Token::OpenPar) {
        token = lexer.getNextToken();
        E();
        if (token == Token::ClosePar) {
            token = lexer.getNextToken();
        } else {
            std::cerr << "Expected )" << std::endl;
        }
    } else {
        throw "Expected number of identifier";
    }
}
