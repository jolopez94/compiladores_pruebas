#ifndef _PARSER_H
#define _PARSER_H

#include "lexer.h"

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    void parse();

private:
    Lexer& lexer;
    Token token;

    void S();
    void ST();
    void E();
    void EP();
    void T();
    void TP();
    void F();
};

#endif
