#include "lexer.h"

Token Lexer::getNextToken() {
    text = "";
    currState = 0;
    getNextSymbol();

    while (1) {
        switch (currState) {
            case 0:
                if (isxdigit(currSymbol)) {
                    text += currSymbol;
                    getNextSymbol();
                    currState = 1;
                } else {
                    currState = 3;
                }
                break;
            case 1:
                if (isxdigit(currSymbol)) {
                    text += currSymbol;
                    getNextSymbol();
                } else if (currSymbol == 'h' || currSymbol == 'H') {
                    text += currSymbol;
                    currState = 2;
                }
                break;
            case 2:
                return Token::Hex;
            case 3:
                if (isdigit(currSymbol)) {
                    text += currSymbol;
                    getNextSymbol();
                    currState = 4;
                } else {
                    currState = 6;
                }
                break;
            case 4:
                if (isdigit(currSymbol)) {
                    text += currSymbol;
                    getNextSymbol();
                } else {
                    ungetSymbol();
                    currState = 5;
                }
                break;
            case 5:
                return Token::Decimal;
            case 6:
                if (currSymbol == '0' || currSymbol == '1') {
                    text += currSymbol;
                    getNextSymbol();
                    currState = 7;
                } else {
                    currState = 9;
                }
                break;
            case 7:
                if (currSymbol == '0' || currSymbol == '1') {
                    text += currSymbol;
                    getNextSymbol();
                } else if (currSymbol == 'b') {
                    text += currSymbol;
                    currState = 8;
                }
                break;
            case 8:
                return Token::Binary;
            case 9:
                if (isOctal()) {
                    text += currSymbol;
                    getNextSymbol();
                    currState = 10;
                } else {
                    currState = 12;
                }
                break;
            case 10:
                if (isOctal()) {
                    text += currSymbol;
                    getNextSymbol();
                } else if (currSymbol == 'o' || currSymbol == 'O') {
                    text += currSymbol;
                    currState = 11;
                } else {

                }
                break;
            case 11:
                return Token::Octal;
            case 12:
                if (currSymbol == ' ' || currSymbol == '\t' || currSymbol == '\n') {
                    getNextSymbol();
                    currState = 0;
                    continue;
                } else if (currSymbol == EOF) {
                    return Token::Eof;
                } else {
                    std::cerr << "Unknown symbol " << text << std::endl;
                    exit(1);
                }
        }
    }
}

bool Lexer::isOctal() {
    if (currSymbol == '0' || currSymbol == '1' || currSymbol == '2' || currSymbol == '3' || currSymbol == '4' || currSymbol == '5' || currSymbol == '6' || currSymbol == '7') {
        return true;
    }
    return false;
}